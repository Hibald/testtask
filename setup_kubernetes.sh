#!/usr/bin/env bash
apt -qq update

# disable swap
swapoff -a
sed -i '/swap/d' /etc/fstab

# bridged traffic to iptables is enabled for kube-route
cat >> /etc/ufw/sysctl.conf <<EOF
net/bridge/bridge-nf-call-ip6tables = 1
net/bridge/bridge-nf-call-iptables = 1
net/bridge/bridge-nf-call-arptables = 1
EOF

# install docker and kubernetes
apt-get -qq install ebtables ethtool apt-transport-https docker.io curl -y
systemctl enable docker
systemctl start docker
#curl -fsSL https://get.docker.com|sh -

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg|apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF

apt-get -qq update
apt-get -qq install kubelet=1.15.4-00 kubeadm=1.15.4-00 kubectl=1.15.4-00 -y
apt-mark  hold kubelet=1.15.4-00 kubeadm=1.15.4-00 kubectl=1.15.4-00


HOSTNAME=$(hostname)
if [[ "${HOSTNAME}" -eq "master" ]]
then
    su vagrant -c '''sudo usermod -a -G docker vagrant
    newgrp docker
    sudo kubeadm init --apiserver-advertise-address=10.0.1.5 --apiserver-cert-extra-sans=10.0.1.5 --pod-network-cidr=10.244.0.0/16 > tmp.log
    tail -2 tmp.log > /vagrant/join.sh
    rm -rf tmp.log

    sudo kubeadm config images pull

    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(whoami): $HOME/.kube/config

    sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/62e44c867a2846fefb68bd5f178daf4da3095ccb/Documentation/kube-flannel.yml
    '''

else
    NODE_IP=$(ifconfig | grep 10.0.1.|awk '{print $2}')
    echo "Environment=\"KUBELET_EXTRA_ARGS=--node-ip=${NODE_IP}\""|sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
    /bin/bash -x /vagrant/join.sh
fi